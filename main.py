import re
import sys
from pathlib import Path
from os import walk

def listeDossier(path = None):
    """Ecris tous les noms des fichiers d'un dossier dans un fichier csv, en séparant les groupes de chiffres"""
    # récupération path
    if path == None:
        path = Path().absolute()
    else:
        path = Path(path)

    # récupération des fichiers
    _, _, filenames = next(walk(path))

    # création du fichier résultat s'il n'existe pas
    resultatPath = path.parent.absolute() / "resultat.csv"
    if not isFileExists(resultatPath):
        open(resultatPath, "x")
    
    # ajout des groupes de chiffres dans les colomnes du csv
    for i in range(0, len(filenames)):
        ID = re.findall(r'\d+', filenames[i])
        for j in range(0, len(ID)):
            filenames[i] = f"{filenames[i]};{ID[j]}"

    # ajout des résultat au fichier
    with open(resultatPath, 'a') as f:
        f.write('\n'.join(filenames))
        f.write('\n')

    return "Done!"

def isFileExists(path):
    """Vérifie qu'un fichier existe"""
    try:
        open(path, "r")
    except FileNotFoundError:
        return False
    else:
        return True

if __name__ == "__main__":
    print(listeDossier(sys.argv[1] if len(sys.argv) > 1 else None))
